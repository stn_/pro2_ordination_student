package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> dosis;

	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
		dosis = new ArrayList<>();
	}

	/**
	 * @return the dosis
	 */
	public ArrayList<Dosis> getDosis() {
		return dosis;
	}

	// TODO : needs tuning!
	public void opretDosis(LocalTime tid, double antal) {
		dosis.add(new Dosis(tid, antal));
	}

	@Override
	public double samletDosis() {
		int samletDosis = 0;
		for (Dosis d : dosis) {
			samletDosis += d.getAntal();
		}
		samletDosis = samletDosis * this.antalDage();
		return samletDosis;
	}

	@Override
	public double doegnDosis() {
		int samletDosis = 0;
		for (Dosis d : dosis) {
			samletDosis += d.getAntal();
		}
		return samletDosis;
	}

	@Override
	public String getType() {
		return super.getLaegemiddel().getNavn();
	}
}

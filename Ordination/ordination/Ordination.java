package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public abstract class Ordination {

	// ---------------------------------------------------------------------
	// FIELDS <-------------->
	private LocalDate startDen;
	private LocalDate slutDen;

	// link to Laegemiddel class (--> 0..1) ---------------
	private Laegemiddel laegemiddel;

	// ----------------------------------------------------------------
	// CONSTRUCTOR <-------------->
	/** Specifikation - som ikke skrevet endnu */
	public Ordination(LocalDate startDen, LocalDate slutDen) {
		this.startDen = startDen;
		this.slutDen = slutDen;
	}

	// --------------------------------------------------------------------
	// GET/SET <-------------->
	public LocalDate getStartDen() {
		return startDen;
	}

	public LocalDate getSlutDen() {
		return slutDen;
	}

	// -------------------------------------------------------------- LINK
	// METHODS <--------------> skal placeres andetsteds
	public Laegemiddel getLaegemiddel() {
		return this.laegemiddel;
	}

	public void setLaegemiddel(Laegemiddel laegemiddel) {
		this.laegemiddel = laegemiddel;
	}

	public void createLaegemiddel(String navn, double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
			double enhedPrKgPrDoegnTung, String enhed) {
		new Laegemiddel(navn, enhedPrKgPrDoegnTung, enhedPrKgPrDoegnTung, enhedPrKgPrDoegnTung, enhed);

	}

	public void clearLaegemiddel() { // since this is a (--> 0..1) association,
										// the ordination can turn this link
										// into null
		this.laegemiddel = null;
	}

	// -------------------------------------------------------------------
	// TOSTRING <-------------->
	@Override
	public String toString() {
		return startDen.toString();
	}

	// -------------------------------------------------------------
	// ADMINISTRATION <-------------->
	/**
	 * Antal hele dage mellem startdato og slutdato. Begge dage inklusive.
	 *
	 * @return antal dage ordinationen gælder for
	 */
	public int antalDage() {
		return (int) ChronoUnit.DAYS.between(startDen, slutDen) + 1;
	}

	/**
	 * Returnerer den totale dosis der er givet i den periode ordinationen er
	 * gyldig
	 *
	 * @return
	 */
	public abstract double samletDosis();

	/**
	 * Returnerer den gennemsnitlige dosis givet pr dag i den periode
	 * ordinationen er gyldig
	 *
	 * @return
	 */
	public abstract double doegnDosis();

	/**
	 * Returnerer ordinationstypen som en String
	 *
	 * @return
	 */
	public abstract String getType();
}

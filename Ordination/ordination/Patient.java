//hej hej

package ordination;

import java.util.ArrayList;

public class Patient {
	// --------------------------------------------------------------------
	// FIELDS <-------------->
	private String cprnr;
	private String navn;
	private double vaegt;

	// link to Ordination class (--> 0..*)
	private ArrayList<Ordination> ordinationer = new ArrayList<>(); // NOTE:
																	// maybe
																	// another
																	// Collection
																	// type
																	// like
																	// HashMap
																	// ?

	// ---------------------------------------------------------------
	// CONSTRUCTOR <-------------->
	public Patient(String cprnr, String navn, double vaegt) {
		this.cprnr = cprnr;
		this.navn = navn;
		this.vaegt = vaegt;
	}

	// -------------------------------------------------------------------
	// GET/SET <-------------->
	public String getCprnr() {
		return cprnr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public double getVaegt() {
		return vaegt;
	}

	public void setVaegt(double vaegt) {
		this.vaegt = vaegt;
	}

	// -------------------------------------------------------------- LINK
	// METHODS <-------------->
	public ArrayList<Ordination> getOrdinationer() {
		return new ArrayList<>(ordinationer);
	}

	public void addOrdination(Ordination ordination) { //
		ordinationer.add(ordination);
	}

	public void removeOrdination(Ordination ordination) {
		ordinationer.remove(ordination);
	}

	// -----------------------------------------------------------------
	// OVERRIDES <-------------->
	@Override
	public String toString() {
		return navn + "  " + cprnr;
	}

}

package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

public class PN extends Ordination {

	// --------------------------------------------------------------------
	// FIELDS <-------------->
	private double antalEnheder; // decided amount of medicine per intake

	// link to Laegemiddel class (--> 0..*) ---------- Collection of the dates
	// of each given dose
	private ArrayList<String> dosisDato = new ArrayList<>();

	// ---------------------------------------------------------------
	// CONSTRUCTOR <-------------->
	public PN(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen);

		this.antalEnheder = antalEnheder;
		System.out.println("Lægemiddelnavn (PN): " + laegemiddel); // Output
																	// test
	}

	// -------------------------------------------------------------------
	// GET/SET <-------------->
	public double getAntalEnheder() {
		return antalEnheder;
	}

	// -------------------------------------------------------------- ADMIN
	// METODS <-------------->
	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		long days = ChronoUnit.DAYS.between(givesDen, getSlutDen());
		if (days >= 0 && days <= antalDage()) { // Check that ordination is
												// still valid
			dosisDato.add(givesDen.toString()); // Register date of dose
			return true;
		}
		return false;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return dosisDato.size(); // Same as length of ArrayList
	}

	// -----------------------------------------------------------------
	// OVERRIDES <-------------->
	@Override
	public double doegnDosis() {
		Collections.sort(dosisDato);
		long days = ChronoUnit.DAYS.between(LocalDate.parse(dosisDato.get(0)),
				LocalDate.parse(dosisDato.get(dosisDato.size() - 1)));
		System.out.println("PN-test: " + samletDosis() + " " + days);
		return samletDosis() / days;
		// (number of times ordination is used * unit count) / (number of days
		// between first and last dose given)
	}

	@Override
	public double samletDosis() {
		return dosisDato.size() * this.antalEnheder; // Same as length of
														// ArrayList times the
														// size of the dose
	}

	@Override
	public String getType() {
		return "Efter behov";
	}

}

package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

	private Dosis[] doser = new Dosis[4];
	Laegemiddel laegemiddel;

	public DagligFast(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	/**
	 * @return the doser
	 */
	public Dosis[] getDoser() {
		return doser;
	}

	@Override
	public double samletDosis() {
		double sum = 0;
		for (int k = 0; k < doser.length; k++) {
			sum += doser[k].getAntal();
		}
		return sum * this.antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (int k = 0; k < doser.length; k++) {
			sum += doser[k].getAntal();
		}
		return sum;
	}

	@Override
	public String getType() {
		return super.getLaegemiddel().getNavn();
	}

	private int counter;

	public void createDose(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		counter = 0;
		doser[counter] = dosis;
		counter++;
	}

	public void createLaegemiddel(Laegemiddel laegemiddel) {
		// TODO Auto-generated method stub

	}
}
